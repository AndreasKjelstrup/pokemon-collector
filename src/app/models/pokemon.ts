import { Ability } from './pokemon-abilities';
import { BaseStats } from './pokemon-baseStats';
import { Move } from './pokemon-moves';
import { PokemonSprite } from './pokemon-sprite';
import { Type } from './pokemon-type';

export interface Pokemon {
    id:number;
    name?:string;
    image?:string;
    url:string;
    types?:Type[];
    moves?:Move[];
    stats?:BaseStats[];
    height:number;
    weight:number;
    abilities:Ability[];
    base_experience:number;
    sprites?:PokemonSprite;
}

