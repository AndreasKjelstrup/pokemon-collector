import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css']
})
export class CataloguePageComponent implements OnInit {

  pokemonList: Pokemon[] = [];

  constructor(private pokemonService: PokemonService, private router: Router) { }

  ngOnInit(): void {
    this.pokemonService.getPokemon().subscribe(pokemonList => {
      this.pokemonList = pokemonList;
    });
  }

  public onPokemonClick(id: number): void {
    this.router.navigateByUrl(`/pokemon/${id}`);
  }

}
