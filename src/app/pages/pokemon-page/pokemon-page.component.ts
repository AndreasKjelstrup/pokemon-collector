import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonDetailService } from 'src/app/services/pokemon-detail.service';

@Component({
  selector: 'app-pokemon-page',
  templateUrl: './pokemon-page.component.html',
  styleUrls: ['./pokemon-page.component.css']
})
export class PokemonPageComponent implements OnInit {

  public pokemonId!: number;
  
  public pokemon!: Pokemon;
  public isLoading = true;

  constructor(private route: ActivatedRoute, private pokemonDetailService: PokemonDetailService) {

    this.pokemonId = this.route.snapshot.params.id;
   }

  ngOnInit(): void {
    this.pokemonDetailService.getPokemon( this.pokemonId ).subscribe(pokemon => {
      this.pokemon = pokemon;
      this.isLoading = false;
    });
  }

}
