import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { PokemonItemComponent } from './components/pokemon-item/pokemon-item.component';
import { CommonModule } from '@angular/common';
import { PokemonPageComponent } from './pages/pokemon-page/pokemon-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    CataloguePageComponent,
    PokemonItemComponent,
    PokemonPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
