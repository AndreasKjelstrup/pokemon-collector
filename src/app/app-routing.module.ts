import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { PokemonPageComponent } from './pages/pokemon-page/pokemon-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'pokemon', component: CataloguePageComponent },
  { path: 'trainer', component: TrainerPageComponent },
  { path: 'pokemon/:id', component: PokemonPageComponent },
  { path: '', pathMatch:'full', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
