import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


import { Pokemon } from '../models/pokemon';


@Injectable({
  providedIn: 'root'
})

export class PokemonService {

  pokemonUrl:string='https://pokeapi.co/api/v2/pokemon';
  pokemonLimit:string = '?limit=850';

  constructor(private http:HttpClient) { }

  // Get Pokemon
  getPokemon():Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`${this.pokemonUrl}${this.pokemonLimit}`).pipe(
      map((resp: any) => {
        return resp.results.map((pokemon: Pokemon) => ({
          ...pokemon,
          ...this.getIdAndImage(pokemon.url)
        }))
      })
    )
  }

  // Private methods - Should maybe be a util file?
  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, 
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

}
