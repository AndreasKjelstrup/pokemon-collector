import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pokemon } from '../models/pokemon';
import { BaseStats } from '../models/pokemon-baseStats';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService {


  constructor(private http: HttpClient) { }

  getPokemon(id:number): Observable<Pokemon> {
    return this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${id}`);
    // .pipe(
    //   map((response: Pokemon) => ({
    //     ...response,
    //     stats: response.stats?.map(this.mapStats),

    //   }))
    // );
  }

  // private mapStats(stat: any):BaseStats {
  //   return {
  //     name: stat.name,
  //     base_stat: stat.base_stat
  //   };
  // }
}
