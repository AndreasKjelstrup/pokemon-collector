# Pokemon Collector

This program is a solution to Task04 of Javascript at Experis Academy Denmark using Angular.

This program functions as a place for a user to create a pokemon trainer, to collect and show information of various Pokémon.

## Installation
Clone this repository, by running the following git commands: 

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:AndreasKjelstrup/pokemon-collector.git
git add .
```

After this is done, run the following commands in a terminal:

```
cd pokemon-trainer
ng serve --open
```

This will start up the server, and open a new browser window with the application.


### Maintainers 
Andreas Kjelstrup

### License
Copyright 2020, Andreas Kjelstrup
